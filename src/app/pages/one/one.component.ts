import { Component, OnInit } from '@angular/core';
import { AdditiveBlending, BufferGeometry, Clock, Color, DirectionalLight, Float32BufferAttribute, Group, HemisphereLight, IcosahedronGeometry, Line, LineBasicMaterial, LineSegments, MathUtils, Mesh, MeshBasicMaterial, MeshLambertMaterial, Object3D, PerspectiveCamera, RingGeometry, Scene, sRGBEncoding, Vector3, WebGLRenderer } from 'three';

import { BoxLineGeometry } from 'three/examples/jsm/geometries/BoxLineGeometry.js';
import { VRButton } from 'three/examples/jsm/webxr/VRButton.js';
import { XRControllerModelFactory } from 'three/examples/jsm/webxr/XRControllerModelFactory.js';
@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent implements OnInit {

  scene: Scene = new Scene();
  camera: PerspectiveCamera = new PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 10 );
  renderer: WebGLRenderer = new WebGLRenderer( { antialias: true } );
  room: LineSegments = new LineSegments(
    new BoxLineGeometry( 6, 6, 6, 10, 10, 10 ),
    new LineBasicMaterial( { color: 0x808080 } )
  );

  controller1: any;
  controller2: any;
  controllerGrip1: any;
  controllerGrip2: any;

  count = 0;
  normal = new Vector3();
  radius = 0.08;
  relativeVelocity = new Vector3();
  clock = new Clock();

  constructor() { }

  ngOnInit(): void {

    this.scene.background = new Color( 0x505050 );

    this.camera.position.set( 0, 1.6, 3 );

    this.room.geometry.translate( 0, 3, 0 );
    this.scene.add( this.room );

    this.scene.add( new HemisphereLight( 0x606060, 0x404040 ) );

    const light = new DirectionalLight( 0xffffff );
    light.position.set( 1, 1, 1 ).normalize();
    this.scene.add( light );

    const geometry = new IcosahedronGeometry( this.radius, 3 );

    for ( let i = 0; i < 200; i ++ ) {

      const object = new Mesh( 
        geometry, 
        new MeshLambertMaterial( 
          { color: Math.random() * 0xffffff } 
        )
      );

      object.position.x = Math.random() * 4 - 2;
      object.position.y = Math.random() * 4;
      object.position.z = Math.random() * 4 - 2;

      object.userData.velocity = new Vector3();
      object.userData.velocity.x = Math.random() * 0.01 - 0.005;
      object.userData.velocity.y = Math.random() * 0.01 - 0.005;
      object.userData.velocity.z = Math.random() * 0.01 - 0.005;

      this.room.add( object );

    }

    //

    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( window.innerWidth, window.innerHeight );
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.xr.enabled = true;
    document.body.appendChild( this.renderer.domElement );

    //

    document.body.appendChild( VRButton.createButton( this.renderer ) );

    // controllers

    const buildController = ( data: { targetRayMode: any; } ) => {

      let geometry, material;
  
      switch ( data.targetRayMode ) {
  
        case 'tracked-pointer':
  
          geometry = new BufferGeometry();
          geometry.setAttribute( 'position', new Float32BufferAttribute( [ 0, 0, 0, 0, 0, - 1 ], 3 ) );
          geometry.setAttribute( 'color', new Float32BufferAttribute( [ 0.5, 0.5, 0.5, 0, 0, 0 ], 3 ) );
  
          material = new LineBasicMaterial( { vertexColors: true, blending: AdditiveBlending } );
  
          return new Line( geometry, material );
  
        case 'gaze':
  
          geometry = new RingGeometry( 0.02, 0.04, 32 ).translate( 0, 0, - 1 );
          material = new MeshBasicMaterial( { opacity: 0.5, transparent: true } );
          return new Mesh( geometry, material );

        default:

          return null;
    
      }
  
    }

    this.controller1 = this.renderer.xr.getController( 0 );
    this.controller1.userData = { isSelecting: false };
    this.controller1.addEventListener( 'selectstart', this.onSelectStart );
    this.controller1.addEventListener( 'selectend', this.onSelectEnd );
    this.controller1.addEventListener( 'connected', ( event: any ) => {

      this.controller1.add( buildController(event.data) );

    });

    this.controller1.addEventListener( 'disconnected',  () => {

      this.controller1.remove( this.controller1.children[0] );

    });

    this.scene.add( this.controller1 );

    this.controller2 = this.renderer.xr.getController( 1 );
    this.controller2.userData = { isSelecting: false };
    this.controller2.addEventListener( 'selectstart', this.onSelectStart );
    this.controller2.addEventListener( 'selectend', this.onSelectEnd );
    this.controller2.addEventListener( 'connected', ( event: { data: any; } ) => {

      this.controller2.add( buildController( event.data ) );

    });

    this.controller2.addEventListener( 'disconnected', () => {

      this.controller2.remove( this.controller2.children[0] );

    });

    this.scene.add( this.controller2 );

    // The XRControllerModelFactory will automatically fetch controller models
    // that match what the user is holding as closely as possible. The models
    // should be attached to the object returned from getControllerGrip in
    // order to match the orientation of the held device.

    const controllerModelFactory = new XRControllerModelFactory();

    this.controllerGrip1 = this.renderer.xr.getControllerGrip( 0 );
    this.controllerGrip1.add( controllerModelFactory.createControllerModel( this.controllerGrip1 ) );
    this.scene.add( this.controllerGrip1 );

    this.controllerGrip2 = this.renderer.xr.getControllerGrip( 1 );
    this.controllerGrip2.add( controllerModelFactory.createControllerModel( this.controllerGrip2 ) );
    this.scene.add( this.controllerGrip2 );

    //

    window.addEventListener( 'resize', this.onWindowResize );
    this.animate()

  }

  onSelectStart = (data: any) => {

    data.target.userData.isSelecting = true;

  }

  onSelectEnd = (data: any) => {

    data.target.userData.isSelecting = false;

  }

  onWindowResize = () => {

    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize( window.innerWidth, window.innerHeight );

  }

  handleController = ( controller: { userData: any; position: any; quaternion: any; } ) => {

    if ( controller.userData.isSelecting ) {

      const object = this.room.children[ this.count ++ ];

      object.position.copy( controller.position );
      object.userData.velocity.x = ( Math.random() - 0.5 ) * 3;
      object.userData.velocity.y = ( Math.random() - 0.5 ) * 3;
      object.userData.velocity.z = ( Math.random() - 9 );
      object.userData.velocity.applyQuaternion( controller.quaternion );

      if ( this.count === this.room.children.length ) this.count = 0;

    }

  }

  animate = () => {

    this.renderer.setAnimationLoop( this.render );

  }

  render = () => {

    this.handleController( this.controller1 );
    this.handleController( this.controller2 );

    //

    const delta = this.clock.getDelta() * 0.8; // slow down simulation
    const range = 3 - this.radius;

    for ( let i = 0; i < this.room.children.length; i ++ ) {

      const object = this.room.children[ i ];

      object.position.x += object.userData.velocity.x * delta;
      object.position.y += object.userData.velocity.y * delta;
      object.position.z += object.userData.velocity.z * delta;

      // keep objects inside room

      if ( object.position.x < - range || object.position.x > range ) {

        object.position.x = MathUtils.clamp( object.position.x, - range, range );
        object.userData.velocity.x = - object.userData.velocity.x;

      }

      if ( object.position.y < this.radius || object.position.y > 6 ) {

        object.position.y = Math.max( object.position.y, this.radius );

        object.userData.velocity.x *= 0.98;
        object.userData.velocity.y = - object.userData.velocity.y * 0.8;
        object.userData.velocity.z *= 0.98;

      }

      if ( object.position.z < - range || object.position.z > range ) {

        object.position.z = MathUtils.clamp( object.position.z, - range, range );
        object.userData.velocity.z = - object.userData.velocity.z;

      }

      for ( let j = i + 1; j < this.room.children.length; j ++ ) {

        const object2 = this.room.children[ j ];

        this.normal.copy( object.position ).sub( object2.position );

        const distance = this.normal.length();

        if ( distance < 2 * this.radius ) {

          this.normal.multiplyScalar( 0.5 * distance - this.radius );

          object.position.sub( this.normal );
          object2.position.add( this.normal );

          this.normal.normalize();

          this.relativeVelocity.copy( object.userData.velocity ).sub( object2.userData.velocity );

          this.normal = this.normal.multiplyScalar( this.relativeVelocity.dot( this.normal ) );

          object.userData.velocity.sub( this.normal );
          object2.userData.velocity.add( this.normal );

        }

      }

      object.userData.velocity.y -= 9.8 * delta;

    }

    this.renderer.render( this.scene, this.camera );

    this.animate();

  }

}